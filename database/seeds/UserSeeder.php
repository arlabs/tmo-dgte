<?php

use App\Profile;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserSeeder extends Seeder{

    public function run() {
        DB::table('users')->truncate();
        DB::table('profiles')->truncate();

        $num = rand(1000,2000);

        //region Users
        $users = [ // Admin
            [
                'email' => 'john.doe@gmail.com',
                'password' => bcrypt('123456'),
                'slug' => Str::slug(1 . ' john doe'),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        ];

        for ($i = 2; $i <= 20; $i++)
        {
            $users[] = [ // Owner/Driver
                'email' => 'user' . $i . '@gmail.com',
                'password' => bcrypt('123456'),
                'slug' => Str::slug($i . ' user' . $num .' user' . $num),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ];
        }

        User::insert($users);
        //endregion

        //region Profiles
        $profiles = [ // Admin
            [
                'user_id' => 1,
                'first_name' => 'john',
                'middle_name' => 'wallace',
                'last_name' => 'doe',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        ];

        for ($i = 2; $i <= 20; $i++)
        {
            $profiles[] = [ // Driver/Owner
                'user_id' => $i,
                'first_name' => 'user' . $num,
                'middle_name' => '',
                'last_name' => 'user' . $num,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ];
        }

        Profile::insert($profiles);
        //endregion
    }

} 