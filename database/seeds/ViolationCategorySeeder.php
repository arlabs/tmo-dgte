<?php

use App\ViolationCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ViolationCategorySeeder extends Seeder{

    public function run()
    {
        DB::table('violation_categories')->truncate();

        $category = [
            [
                'name' => 'Traffic Violation Citation Ticket',
                'slug' => Str::slug('traffic violation citation ticket'),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'Motor Vehicle Impounding Receipt',
                'slug' => Str::slug('motor vehicle impounding receipt'),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        ];

        ViolationCategory::insert($category);
    }

} 