<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		 $this->call('RoleSeeder');
		 $this->call('ViolationCategorySeeder');
		 $this->call('UserSeeder');
		 $this->call('UserRoleSeeder');
		 $this->call('ViolationSeeder');
	}

}
