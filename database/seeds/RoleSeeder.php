<?php
use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder{

    public function run(){

        DB::table('roles')->truncate();

        $roles = [
            [
                'name' => 'driver',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'owner',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'admin',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        ];

        Role::insert($roles);
    }

} 