<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRoleSeeder extends Seeder
{

    public function run()
    {
        DB::table('user_roles')->truncate();

        for ($i = 1; $i <= 20; $i++)
        {
            // Driver/Owner Role
            if ($i >= 2)
            {
                $user = User::with('roles')->where('id','=',$i)->first();
                $user->roles()->sync([1,2]);
            }
            else
            {
                // Admin Role
                $user = User::with('roles')->where('id','=',1)->first();
                $user->roles()->sync([3]);
            }
        }
    }

} 