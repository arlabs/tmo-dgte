<?php

use App\Violation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ViolationSeeder extends Seeder
{

    public function run()
    {
        DB::table('violations')->truncate();

        $violations = [];

        for ($i = 1; $i <= 3; $i++)
        {
            $num1 = rand(500,2000);
            $num2 = rand(500,2000);
            $violations[] = [
                'violation_category_id' => 1,
                'name' => 'Violation ' . $num1,
                'description' => '',
                'penalty' => $num1,
                'slug' => Str::slug($i . ' violation ' . $num1),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ];

            $violations[] = [
                'violation_category_id' => 2,
                'name' => 'Violation ' . $num2,
                'description' => '',
                'penalty' => $num2,
                'slug' => Str::slug($i . ' violation ' . $num2),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ];
        }

        Violation::insert($violations);
    }

} 