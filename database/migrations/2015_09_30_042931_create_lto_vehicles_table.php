<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLtoVehiclesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('lto_vehicles', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('registration_number');
            $table->string('official_receipt');
            $table->string('type');
            $table->string('make');
            $table->string('plate_number');
            $table->string('bar_code');
            $table->string('year_model');
            $table->string('motor_number');
            $table->string('chassis_number');
            $table->string('dlr_number');
            $table->timestamps();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('lto_vehicles');
	}

}
