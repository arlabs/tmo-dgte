<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserViolationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_violations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('violation_id');
            $table->string('location');
            $table->dateTime('happened_at');
            $table->enum('status',['unpaid','paid']);
            $table->string('latitude');
            $table->string('longitude');
            $table->timestamps();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_violations');
	}

}
