-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 04, 2015 at 01:56 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tmo`
--

-- --------------------------------------------------------

--
-- Table structure for table `lto_profiles`
--

CREATE TABLE IF NOT EXISTS `lto_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `license_type` enum('student permit','non-professional','professional') COLLATE utf8_unicode_ci NOT NULL,
  `license_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `license_date_issued` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `lto_profiles`
--

INSERT INTO `lto_profiles` (`id`, `first_name`, `middle_name`, `last_name`, `telephone_number`, `address`, `license_type`, `license_number`, `license_date_issued`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Danilo', 'Credo', 'Lu-ang', '09355756463', '512 Bonifacio ST, Zamboanguita, Negros Oriental', 'student permit', '123456', '2015-10-04 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:21:24');

-- --------------------------------------------------------

--
-- Table structure for table `lto_users`
--

CREATE TABLE IF NOT EXISTS `lto_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lto_users_email_unique` (`email`),
  UNIQUE KEY `lto_users_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `lto_users`
--

INSERT INTO `lto_users` (`id`, `email`, `password`, `slug`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'luangdanilo@gmail.com', 'danilo23', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:16:31');

-- --------------------------------------------------------

--
-- Table structure for table `lto_vehicles`
--

CREATE TABLE IF NOT EXISTS `lto_vehicles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `registration_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `official_receipt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `make` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plate_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bar_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year_model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `motor_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chassis_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dlr_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `lto_vehicles`
--

INSERT INTO `lto_vehicles` (`id`, `user_id`, `registration_number`, `official_receipt`, `type`, `make`, `plate_number`, `bar_code`, `year_model`, `motor_number`, `chassis_number`, `dlr_number`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '201102646', '228565', 'Suzuki Raider R150', 'Underbone', 'KYO075', '9789717913230', '2015', '125648', '654987', '654123', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:16:04');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_09_16_084738_create_profiles_table', 1),
('2015_09_16_090746_create_roles_table', 1),
('2015_09_16_090756_create_user_roles_table', 1),
('2015_09_17_025446_create_violation_categories_table', 1),
('2015_09_17_084947_create_violations_table', 1),
('2015_09_18_135816_create_user_violations_table', 1),
('2015_09_19_032350_create_vehicles_table', 1),
('2015_09_30_041620_create_lto_users_table', 1),
('2015_09_30_041644_create_lto_profiles_table', 1),
('2015_09_30_042931_create_lto_vehicles_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `license_type` enum('student permit','non-professional','professional') COLLATE utf8_unicode_ci NOT NULL,
  `license_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `license_date_issued` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `first_name`, `middle_name`, `last_name`, `telephone_number`, `address`, `license_type`, `license_number`, `license_date_issued`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'john', '', 'doe', '', '', 'student permit', '', '0000-00-00 00:00:00', '2015-09-29 21:23:36', '2015-09-29 21:23:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'driver', '2015-09-29 21:23:35', '2015-09-29 21:23:35', NULL),
(2, 'owner', '2015-09-29 21:23:35', '2015-09-29 21:23:35', NULL),
(3, 'admin', '2015-09-29 21:23:35', '2015-09-29 21:23:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `slug`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'john.doe@gmail.com', '$2y$10$oPRHKcNnKhJ1GdN5gusUreHgjpolj6fXAwS97Hy./lMEjCMnjd7KK', '1 john doe', NULL, '2015-09-29 21:23:35', '2015-09-29 21:23:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_violations`
--

CREATE TABLE IF NOT EXISTS `user_violations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `violation_id` int(11) NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `happened_at` datetime NOT NULL,
  `status` enum('unpaid','paid') COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=107 ;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `violation_id` int(11) NOT NULL,
  `registration_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `official_receipt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `make` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plate_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bar_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year_model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `motor_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chassis_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dlr_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `violations`
--

CREATE TABLE IF NOT EXISTS `violations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `violation_category_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `penalty` double(8,2) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `violations_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

--
-- Dumping data for table `violations`
--

INSERT INTO `violations` (`id`, `violation_category_id`, `name`, `description`, `penalty`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Disregarding Traffic Sign', NULL, 100.00, '1', '2015-10-03 16:00:00', '2015-10-03 16:00:00', '2015-10-04 07:24:12'),
(2, 1, 'Parking on the Sidewalk', NULL, 100.00, '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:29:06'),
(3, 1, 'Obstruction', NULL, 100.00, '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:29:40'),
(4, 1, 'Refusal to Convey', NULL, 100.00, '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:30:50'),
(5, 1, 'Parking Infront of a Driveway', NULL, 100.00, '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:30:50'),
(6, 1, 'Student Driver not Accompanied by Professional Driver', NULL, 100.00, '6', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:32:12'),
(7, 1, 'Illegal Parking', NULL, 100.00, '7', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:32:12'),
(8, 1, 'Plate Improperly Displayed', NULL, 100.00, '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:35:16'),
(9, 1, 'Failure To Bring License', NULL, 500.00, '9', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:35:16'),
(10, 1, 'Discourtesy/Arrogance', NULL, 100.00, '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:35:16'),
(11, 1, 'No Mayor''s Permit (Driver)', NULL, 500.00, '11', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:35:16'),
(12, 1, 'Driving in Slippers/Sleeveless Shirt', NULL, 100.00, '12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:35:16'),
(13, 2, 'Defective Muffler/No Muffler', NULL, 100.00, '13', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:37:12'),
(14, 2, 'No Headlight', NULL, 100.00, '14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:37:12'),
(15, 2, 'No Tail Light', NULL, 100.00, '15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:39:40'),
(16, 2, 'No Signal Light', NULL, 100.00, '16', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:39:40'),
(17, 2, 'No Plate Attached', NULL, 100.00, '17', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:39:40'),
(18, 2, 'No Breke Light', NULL, 100.00, '18', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:39:40'),
(19, 2, 'Unlincesed Driver/Expired DL', NULL, 500.00, '19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:39:40'),
(20, 2, 'Unregistered MV', NULL, 200.00, '20', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:40:34'),
(21, 2, 'Delinquent CR/OR', NULL, 100.00, '21', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-10-04 07:40:34');

-- --------------------------------------------------------

--
-- Table structure for table `violation_categories`
--

CREATE TABLE IF NOT EXISTS `violation_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `violation_categories_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `violation_categories`
--

INSERT INTO `violation_categories` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Traffic Violation Citation Ticket', 'traffic-violation-citation-ticket', '2015-09-29 21:23:35', '2015-09-29 21:23:35', NULL),
(2, 'Motor Vehicle Impounding Receipt', 'motor-vehicle-impounding-receipt', '2015-09-29 21:23:35', '2015-09-29 21:23:35', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
