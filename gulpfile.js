var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix
        .styles([
            'vendor/bootstrap.min.css',
            'vendor/datetimepicker/bootstrap-datetimepicker.min.css',
            'vendor/font-awesome.min.css',
            'vendor/datatables/dataTables.bootstrap.css'
        ], './public/css/libs.css')
        .styles([
            'front/bootstrap-theme.css',
            'front/main.css',
            'front/custom.css'
        ], './public/css/front.css')
        .styles([
            'back/AdminLTE.css',
            'back/skin-orange.min.css',
            /*'back/iCheck/orange.css',*/
            'back/custom.css'
        ], './public/css/back.css');

    mix
        .scripts([
            'vendor/bootstrap.min.js',
            'vendor/datetimepicker/moment.js',
            'vendor/datetimepicker/bootstrap-datetimepicker.min.js',
            'vendor/datatables/jquery.dataTables.js',
            'vendor/datatables/dataTables.bootstrap.js'
        ], './public/js/libs.js')
        .scripts([
            'front/headroom.min.js',
            'front/jQuery.headroom.min.js',
            'front/template.js',
            'custom.js'
        ], './public/js/front.js')
        .scripts([
            /*'back/icheck.min.js',*/
            'back/jquery.slimscroll.min.js',
            'back/fastclick.min.js',
            'back/app.min.js',
            'custom.js'
        ], './public/js/back.js');

    mix.version(['css/libs.css','css/front.css','css/back.css','js/libs.js','js/front.js','js/back.js']);

});
