<?php namespace App\Handlers\Events;

use App\Events\UserWasIssuedViolation;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class EmailViolationNotification implements ShouldBeQueued
{

    private $mailer;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct(Mailer $mailer)
	{
		$this->mailer = $mailer;
	}

	/**
	 * Handle the event.
	 *
	 * @param  UserWasIssuedViolation  $event
	 * @return void
	 */
	public function handle(UserWasIssuedViolation $event)
	{
        $user = $event->user;

        $data = [
            'name' => ucwords($user->profile->first_name . ' ' . $user->profile->last_name)
        ];

        $this->mailer->queue('emails.violation', $data, function($message) use($user)
        {
            $message->to($user->email, ucwords($user->profile->first_name.' '.$user->profile->last_name))
                ->subject('Issued Violation [TMO - Dumaguete]');
        });
	}

}
