<?php namespace App\Handlers\Commands;

use App\Commands\IssueViolationCommand;
use App\Events\UserWasIssuedViolation;
use App\User;
use App\Vehicle;

class IssueViolationCommandHandler {

	/**
	 * Handle the command.
	 *
	 * @param  IssueViolationCommand  $command
	 * @return void
	 */
	public function handle(IssueViolationCommand $command)
	{
        $user = User::find($command->user_id);

        $user->violations()->attach($command->violation_id, [
            'location' => $command->location,
            'created_at' => date('Y-m-d H:i:s', strtotime($command->happened_at)),
            'status' => $command->status
        ]);

        Vehicle::create([
            'user_id' => $command->user_id,
            'violation_id' => $command->violation_id,
            'registration_number' => $command->registration_number,
            'official_receipt' => $command->official_receipt,
            'type' => $command->type,
            'make' => $command->make,
            'plate_number' => $command->plate_number,
            'bar_code' => $command->bar_code,
            'year_model' => $command->year_model,
            'motor_number' => $command->motor_number,
            'chassis_number' => $command->chassis_number,
            'dlr_number' => $command->dlr_number,
        ]);

        event(new UserWasIssuedViolation($user));
	}

}
