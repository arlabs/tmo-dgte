<?php namespace App\Handlers\Commands;

use App\Commands\CreateNotificationCommand;

use App\Notification;
use App\User;
use Illuminate\Queue\InteractsWithQueue;

class CreateNotificationCommandHandler {

	/**
	 * Handle the command.
	 *
	 * @param  CreateNotificationCommand  $command
	 * @return void
	 */
	public function handle(CreateNotificationCommand $command)
	{
        $user = User::find($command->user_id);

        Notification::create([
            'content' => $user->profile->first_name.' '.$user->profile->last_name.' has been issued a violation.',
            'is_new' => 1
        ]);
	}

}
