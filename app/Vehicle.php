<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{

    protected $fillable = [
        'user_id',
        'violation_id',
        'registration_number',
        'official_receipt',
        'type',
        'make',
        'plate_number',
        'bar_code',
        'year_model',
        'motor_number',
        'chassis_number',
        'dlr_number'
    ];

} 