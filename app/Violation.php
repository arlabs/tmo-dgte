<?php
namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Violation extends Model
{
    use SoftDeletes;

    protected $fillable = ['violation_category_id','name','description','penalty','slug'];

    public function users()
    {
        return $this->belongsToMany('App\User','user_violations')
            ->withPivot('location','status')
            ->withTimestamps();
    }

    public function category()
    {
        return $this->belongsTo('App\ViolationCategory','violation_category_id');
    }

    public function vehicle()
    {
        return $this->hasOne('App\Vehicle');
    }

    public function scopeCategorize($query, $categoryId, Request $request)
    {
        if ($request->has('cat')) {
            $query->where('violation_category_id', '=', $categoryId);
        }
    }

    /**
     * Post action to store / update a violation.
     *
     * @param Request $request
     * @param bool $isUpdate
     * @param null $slug
     * @return static
     */
    public static function storeOrUpdate(Request $request, $isUpdate = false, $slug = null)
    {
        if ($isUpdate)
        {
            $violation = Violation::where('slug','=',$slug)->first();
            $violation->violation_category_id = $request->get('violation_category');
            $violation->name = $request->get('name');
            $violation->penalty = $request->get('penalty');
            $violation->description = $request->get('description');
            $violation->save();
        }
        else
        {
            $violation = Violation::create([
                'violation_category_id' => $request->get('violation_category'),
                'name' => $request->get('name'),
                'penalty' => $request->get('penalty'),
                'description' => $request->get('description')
            ]);
        }

        // Slug
        $violation->slug = Str::slug($violation->id.' '.$violation->name);
        $violation->save();

        return $violation;
    }

} 