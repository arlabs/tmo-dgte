<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ViolationCategory extends Model{

    protected $fillable = ['name','slug'];

    protected $table = 'violation_categories';

    public function violations()
    {
        return $this->hasMany('App\Violation','violation_category_id');
    }

    /**
     * Get categorized list of violations.
     *
     * @return array
     */
    public static function getCategorizedList()
    {
        $violation_list = [];

        $categories = ViolationCategory::all();
        if ($categories->count()) {
            foreach ($categories as $category) {
                $violation_list[ucwords($category->name)] = $category->violations->lists('name','id');
            }
        }

        return $violation_list;
    }

} 