<?php namespace App\Commands;

use App\Commands\Command;
use Illuminate\Http\Request;

class CreateNotificationCommand extends Command {

    public $user_id;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(Request $request)
	{
        $this->user_id = $request->get('user');
	}

}
