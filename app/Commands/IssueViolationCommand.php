<?php namespace App\Commands;

use App\Http\Requests\CreateIssueViolationRequest;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;

class IssueViolationCommand extends Command implements ShouldBeQueued
{
    use SerializesModels;

    public $violation_id;
    public $location;
    public $happened_at;
    public $status;

    public $user_id;
    public $registration_number;
    public $official_receipt;
    public $type;
    public $make;
    public $plate_number;
    public $bar_code;
    public $year_model;
    public $motor_number;
    public $chassis_number;
    public $dlr_number;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(CreateIssueViolationRequest $request)
	{
        $this->violation_id = $request->get('violation');
        $this->location = $request->get('location');
        $this->happened_at = $request->get('happened_at');
        $this->status = $request->get('status');

        $this->user_id = $request->get('user');
        $this->registration_number = $request->get('registration_number');
        $this->official_receipt = $request->get('official_receipt');
        $this->type = $request->get('type');
        $this->make = $request->get('make');
        $this->plate_number = $request->get('plate_number');
        $this->bar_code = $request->get('bar_code');
        $this->year_model = $request->get('year_model');
        $this->motor_number = $request->get('motor_number');
        $this->chassis_number = $request->get('chassis_number');
        $this->dlr_number = $request->get('dlr_number');
	}

}
