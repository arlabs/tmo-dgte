<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class Profile extends Model
{

    protected $fillable = [
        'user_id',
        'first_name',
        'middle_name',
        'last_name',
        'telephone_number',
        'address',
        'license_type',
        'license_number',
        'license_date_issued'
    ];

    protected $dates = ['license_date_issued'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function doUpdate(Request $request, $slug)
    {
        $user = User::with(['profile'])->where('slug','=',$slug)->first();
        $user->profile->first_name = $request->get('first_name');
        $user->profile->middle_name = $request->get('middle_name');
        $user->profile->last_name = $request->get('last_name');
        $user->profile->telephone_number = $request->get('telephone_number');
        $user->profile->address = $request->get('address');
        $user->profile->license_type = $request->get('license_type');
        $user->profile->license_number = $request->get('license_number');
        $user->profile->license_date_issued = date('Y-m-d H:i:s', strtotime($request->get('license_date_issued')));
        $user->profile->save();

        // User slug
        $user->slug = Str::slug($user->id . ' ' . $request->get('first_name') . ' ' . $request->get('last_name'));
        $user->save();

        // Role
        $user->roles()->sync($request->get('role'));

        return $user;
    }

} 