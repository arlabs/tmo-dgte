<?php

Route::get('/', 'PagesController@index');
Route::get('violation-information', 'PagesController@violations');
Route::get('about', 'PagesController@about');
Route::get('faq', 'PagesController@faq');
Route::get('contact', 'PagesController@contact');

Route::get('traffic', 'GoogleController@traffic');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::group(['middleware' => 'auth'], function() {
    Route::get('dashboard', 'DashboardController@index');

    // Users
    Route::group(['prefix' => 'users'], function() {
        Route::get('/', 'UsersController@index');
        Route::get('create/{purpose}', 'UsersController@create');
        Route::post('/', 'UsersController@store');
        Route::get('{slug}', 'UsersController@show');
        Route::post('{slug}', 'UsersController@update');
        Route::get('{slug}/load_modal', 'UsersController@load_modal');
        Route::delete('{slug}', 'UsersController@destroy');
    });

    // Traffic
    Route::get('traffic', 'GoogleController@traffic');

    // Violations
    Route::group(['prefix' => 'violations'], function() {
        Route::get('/', 'ViolationsController@index');
        Route::get('table', 'ViolationsController@table');
        Route::get('create', 'ViolationsController@create');
        Route::post('/', 'ViolationsController@store');
        Route::get('{slug}', 'ViolationsController@show');
        Route::post('{slug}', 'ViolationsController@update');
        Route::get('{slug}/load_modal', 'ViolationsController@load_modal');
        Route::delete('{slug}', 'ViolationsController@destroy');
        Route::get('user/issue', 'ViolationsController@issue');
        Route::post('user/issue', 'ViolationsController@post_issue');
        Route::get('user/print_violation', 'ViolationsController@print_violation');
    });

    // Profiles
    Route::group(['prefix' => 'profile'], function() {
        Route::get('/', 'ProfilesController@index');
        Route::get('edit', 'ProfilesController@edit');
        Route::post('update', 'ProfilesController@update');
    });

    // Notifications
    Route::group(['prefix' => 'notifications'], function() {
        Route::get('get_all', 'NotificationsController@getAll');
        Route::get('new_counter', 'NotificationsController@newCounter');
        Route::post('read_all', 'NotificationsController@readAll');
    });

    // Test routes
    Route::get('test', 'TestController@index');
});