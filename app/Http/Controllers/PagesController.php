<?php namespace App\Http\Controllers;

class PagesController extends Controller
{

	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
    {
		return view('front-pages.welcome');
	}

    /**
     * Show the violation page.
     *
     * @return \Illuminate\View\View
     */
    public function violations()
    {
        return view('front-pages.violations');
    }

    /**
     * Show the about page.
     *
     * @return \Illuminate\View\View
     */
    public function about()
    {
        return view('front-pages.about');
    }

    /**
     * Show the FAQ page.
     *
     * @return \Illuminate\View\View
     */
    public function faq()
    {
        return view('front-pages.faq');
    }

    /**
     * Show the contact page.
     *
     * @return \Illuminate\View\View
     */
    public function contact()
    {
        return view('front-pages.contact');
    }
}
