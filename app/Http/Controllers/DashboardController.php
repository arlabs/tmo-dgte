<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        if(Auth::user()->isAdmin() === false)
        {
            Auth::logout();
            Session::flush();
            redirect('/');
        }

        $title = 'Dashboard';

		return view('dashboard', compact('title'));
	}

}
