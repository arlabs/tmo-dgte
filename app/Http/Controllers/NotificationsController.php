<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class NotificationsController extends Controller {

	public function getAll()
    {
        $notifications = Notification::all();

        return View::make('notifications.all', ['notifications' => $notifications])->render();
    }

    public function newCounter()
    {
        return Notification::where('is_new','=',1)->count();
    }

    public function readAll()
    {
        Notification::where('is_new','=',1)->update(['is_new' => 0]);
    }

}
