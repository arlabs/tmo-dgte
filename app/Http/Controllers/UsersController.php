<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Profile;
use App\Role;
use App\User;

class UsersController extends Controller {

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * View list of users.
     *
     * @return \Illuminate\View\View
     */
	public function index()
    {
        $users = User::latest('created_at')
            ->with(['profile','roles'])
            ->where('id','!=',auth()->id())
            ->paginate(10);

        $title = 'Users';

        return view('users.index', compact('users','title'));
    }

    /**
     * Page to create / add a user.
     *
     * @param $purpose
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create($purpose)
    {
        $user = null;
        if ( ! in_array($purpose, ['new_user','new_user_violation']))
        {
            return redirect()->back()->with([
                'alert-level' => 'warning',
                'message' => '<strong>Warning!</strong> You visited unknown url.'
            ]);
        }

        $roles = Role::all();

        $title = 'Register User';

        return view('users.create', compact('user','purpose','roles','title'));
    }

    /**
     * Post action to store user details.
     *
     * @param CreateUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateUserRequest $request)
    {
        User::doStore($request);

        $path = 'users';

        if ($request->get('purpose') == 'new_user_violation')
        {
            $path = 'violations/user/issue';
        }

        return redirect($path)->with([
            'alert-level' => 'success',
            'message' => '<strong>Success!</strong> User has been successfully added.'
        ]);
    }

    /**
     * Page to show a specific user.
     *
     * @param $slug
     * @return \Illuminate\View\View
     */
    public function show($slug)
    {
        $user = User::where('slug','=',$slug)->firstOrFail();

        $purpose = 'edit_user';

        $roles = Role::all();

        return view('users.show', compact('user','purpose','roles'));
    }

    /**
     * Post action to update user details.
     *
     * @param UpdateUserRequest $request
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateUserRequest $request, $slug)
    {
        User::where('slug','=',$slug)->firstOrFail();

        $user = Profile::doUpdate($request, $slug);

        return redirect('users/'.$user->slug)->with([
            'alert-level' => 'success',
            'message' => '<strong>Success!</strong> User has been successfully updated.'
        ]);
    }

    /**
     * Returns a view to modal to delete a user.
     *
     * @param $slug
     * @return mixed
     */
    public function load_modal($slug)
    {
        $user = User::where('slug','=',$slug)->firstOrFail();

        $view = view()->make('users._delete', ['user' => $user])->render();

        return $view;
    }

    /**
     * Post action to delete a user.
     *
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($slug)
    {
        $user = User::where('slug','=',$slug)->firstOrFail();

        $user->delete();

        return redirect('users')->with([
            'alert-level' => 'success',
            'message' => '<strong>Success!</strong> User has been successfully deleted.'
        ]);
    }

}
