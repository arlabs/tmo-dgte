<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class GoogleController extends Controller
{

    /**
     * Page to view google traffic page.
     *
     * @return \Illuminate\View\View
     */
    public function traffic()
    {
        return view('google.traffic');
    }

}
