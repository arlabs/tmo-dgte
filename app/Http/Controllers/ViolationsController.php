<?php namespace App\Http\Controllers;

use App\Commands\CreateNotificationCommand;
use App\Commands\IssueViolationCommand;
use App\Http\Requests;
use App\Http\Requests\CreateIssueViolationRequest;
use App\Http\Requests\CreateViolationsRequest;
use App\User;
use App\Vehicle;
use App\Violation;
use App\ViolationCategory;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class ViolationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin',['except' => ['index','show','print_violation']]);
    }

    /**
     * View list of violations.
     *
     * @return \Illuminate\View\View
     */
	public function index()
    {
        $categories = ViolationCategory::all();

        $title = 'Violations';

        if( ! Auth::user()->isAdmin())
        {
            $violations = Violation::with(['users','category'])
                ->with(['users'])
                ->whereHas('users', function($query){
                    $query->where('users.slug','=',Auth::user()->slug);
                })
                ->orderBy('id','DESC')
                ->paginate(10);
        }

        // Append current query strings
        //$violations->appends(['cat' => $category->slug]);

        return view('violations.index', compact('violations','categories','title'));
    }

    public function table(Request $request)
    {
        $category = ViolationCategory::where(
            function($query) use($request) {
                if ($request->has('cat'))
                {
                    $query->where('slug',$request->get('cat'));
                }
            })
            ->firstOrFail();

        if (Auth::user()->isAdmin())
        {
            $violations = Violation::with(['users','category'])
                ->orderBy('id','DESC')
                ->categorize($category->id, $request)
                ->get();
        }

        $view = View::make('violations._table', ['violations' => $violations]);

        $content = $view->render();

        return $content;
    }

    /**
     * Page to create / add a violation.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $violation = null;

        $categories = ViolationCategory::lists('name','id');

        $title = 'Add Violation';

        return view('violations.create', compact('violation','categories','title'));
    }

    /**
     * Post action to store new violation.
     *
     * @param CreateViolationsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateViolationsRequest $request)
    {
        Violation::storeOrUpdate($request);

        return redirect('violations')->with([
            'alert-level' => 'success',
            'message' => '<strong>Success!</strong> Violation has been successfully registered.'
        ]);
    }

    /**
     * Page to show specific violation.
     *
     * @param $slug
     * @return \Illuminate\View\View
     */
    public function show($slug)
    {
        $categories = ViolationCategory::lists('name','id');

        $violation = Violation::where('slug','=',$slug)->firstOrFail();

        $title = ucwords($violation->name);

        return view('violations.show', compact('violation','users','categories','title'));
    }

    /**
     * Post action to update specific violation.
     *
     * @param CreateViolationsRequest $request
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(CreateViolationsRequest $request, $slug)
    {
        Violation::where('slug','=',$slug)->firstOrFail();

        $violation = Violation::storeOrUpdate($request, true, $slug);

        return redirect('violations')->with([
            'alert-level' => 'success',
            'message' => '<strong>Success!</strong> Violation has been successfully updated.'
        ]);
    }

    /**
     * Returns a view to modal to delete a violation.
     *
     * @param $slug
     * @return string
     */
    public function load_modal($slug)
    {
        $violation = Violation::where('slug','=',$slug)->firstOrFail();

        $view = view()->make('violations._delete', ['violation' => $violation])->render();

        return $view;
    }

    /**
     * Post action to delete a violation.
     *
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($slug)
    {
        $violation = Violation::where('slug','=',$slug)->firstOrFail();

        $violation->delete();

        return redirect('violations')->with([
            'alert-level' => 'success',
            'message' => '<strong>Success!</strong> Violation has been successfully deleted.'
        ]);
    }

    /**
     * Page to issue violation to a user.
     *
     * @return \Illuminate\View\View
     */
    public function issue()
    {
        $categorizedViolationList = ViolationCategory::getCategorizedList();

        $users = User::getAllUsersExcept('admin');

        $title = 'Issue Violation';

        return view('violations.issue', compact('categorizedViolationList','users','title'));
    }

    /**
     * Post action to issue violation to a user.
     *
     * @param CreateIssueViolationRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function post_issue(CreateIssueViolationRequest $request)
    {
        $this->dispatch(new IssueViolationCommand($request));

        $this->dispatch(new CreateNotificationCommand($request));

        return redirect('violations')->with([
            'alert-level' => 'success',
            'message' => '<strong>Success!</strong> User has been successfully issued a violation.'
        ]);
    }

    /**
     * Print violation details of a user.
     *
     * @param Request $request
     * @return mixed
     */
    public function print_violation(Request $request)
    {
        $pdf = app()->make('dompdf.wrapper');

        $user_id = $request->get('user_id');
        $violation_id = $request->get('violation_id');

        // Get user
        $user = User::find($user_id);

        // Get violation
        $violation = $user->violations()->where('violation_id','=',$violation_id)->first();

        // Get vehicle
        $vehicle = Vehicle::where('user_id',$user_id)->where('violation_id',$violation_id)->first();

        $view = view()->make('pdf.ticket-receipt', ['user' => $user, 'violation' => $violation, 'vehicle' => $vehicle])->render();

        $pdf->loadHTML($view);

        return $pdf->stream();
        //return $pdf->download('invoice.pdf');
    }
}
