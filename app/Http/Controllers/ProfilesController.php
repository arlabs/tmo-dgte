<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\UpdateProfileRequest;
use App\Profile;
use App\Role;
use Illuminate\Support\Facades\Auth;

class ProfilesController extends Controller
{

    /**
     * Page to view profile details.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $title = 'Profile';

        $user = auth()->user();

        return view('profiles.index', compact('user','title'));
    }

    /**
     * Page to edit profile details.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        $purpose = 'edit_profile';

        $user = auth()->user();

        $roles = Role::all();

        $title = ucwords(Auth::user()->profile->first_name.' '.Auth::user()->profile->last_name);

        return view('profiles.edit', compact('purpose','user','roles','profile','title'));
    }

    /**
     * Post action to update profile details.
     *
     * @param UpdateProfileRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(UpdateProfileRequest $request)
    {
        Profile::doUpdate($request,auth()->user()->slug);

        return redirect('profile')->with([
            'alert-level' => 'success',
            'message' => '<strong>Success!</strong> Profile has been successfully updated.'
        ]);
    }

}
