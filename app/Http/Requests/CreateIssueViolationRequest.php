<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateIssueViolationRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'violation'             => 'required|numeric',
            'user'                  => 'required|numeric',
            'location'              => 'required',
            'happened_at'           => 'required|date',
            'status'                => 'required',
            'registration_number'   => 'required',
            'official_receipt'      => 'required',
            'type'                  => 'required',
            'make'                  => 'required',
            'plate_number'          => 'required',
            'bar_code'              => 'required',
            'year_model'            => 'required',
            'motor_number'          => 'required',
            'chassis_number'        => 'required',
            'dlr_number'            => 'required'
		];
	}

}
