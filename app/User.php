<?php namespace App;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{

	use Authenticatable, CanResetPassword;
    use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['email', 'password', 'slug'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role','user_roles');
    }

    public function violations()
    {
        return $this->belongsToMany('App\Violation','user_violations')
            ->withPivot('location','happened_at','status')
            ->withTimestamps();
    }

    public function vehicles()
    {
        return $this->hasMany('App\Vehicle','user_id');
    }

    /**
     * Check if user's role includes admin.
     *
     * @return bool
     */
    public function isAdmin()
    {
        $roles = $this->roles->lists('name');

        return !! in_array('admin', $roles);
    }

    /**
     * Get all users except admin.
     * This list is for issuing a violation.
     *
     * @param $role
     * @return array
     */
    public static function getAllUsersExcept($role)
    {
        return User::with(['profile','roles'])
            ->whereHas('roles', function($query) use($role)
            {
                $query->where('roles.name','!=',$role);
            })
            ->lists('email','id');
    }

    /**
     * Post action to store / update user details.
     *
     * @param Request $request
     * @param string $purpose
     * @param null $slug
     * @return Model|null|static
     */
    public static function doStore(Request $request)
    {
        $user = User::create([
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password'))
        ]);

        Profile::create([
            'user_id' => $user->id,
            'first_name' => $request->get('first_name'),
            'middle_name' => $request->get('middle_name'),
            'last_name' => $request->get('last_name'),
            'telephone_number' => $request->get('telephone_number'),
            'address' => $request->get('address'),
            'license_type' => $request->get('license_type'),
            'license_number' => $request->get('license_number'),
            'license_date_issued' => date('Y-m-d H:i:s', strtotime($request->get('license_date_issued')))
        ]);

        // User slug
        $user->slug = Str::slug($user->id . ' ' . $request->get('first_name') . ' ' . $request->get('last_name'));
        $user->save();

        // Role
        $user->roles()->sync($request->get('role'));

        return $user;
    }

}
