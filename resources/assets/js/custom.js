var currentCategory = '';

// Initialize modals
var smallModal = $('.small-modal');
var smallModalContent = $('.modal-content-small');
var modalTitle = $('.modal-title');

$(function () {

    // Disable submit buttons every time form is submitted
    $('button[type=submit]').on('click', function() {
        $(this).addClass('disabled');
    });

    // Initialize tooltip
    $('[data-toggle="tooltip"]').tooltip();

    // Date-Time picker
    $('#happened_at, #license_date_issued').datetimepicker({
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down",
            next: "fa fa-chevron-right",
            previous: "fa fa-chevron-left"
        }
    });

    // Load violations by category
    $('.li-violation-category').on('change', loadViolationByCategory);

    // Load violations on page load
    getViolationTable('', true);

    // Load violations every 5 seconds
    setInterval(function() {
        getViolationTable(currentCategory,false);
    }, 5000);

    // Show delete modal for violation
    $('#violation-list').on('click', '.btn-delete-violation', showDeleteModalViolation);

    // Show delete modal for user
    var btnDeleteUser = $('.btn-delete-user');
    btnDeleteUser.on('click', showDeleteModalUser);

    // Get all notifications
    notificationGetAll();

    // Get all notifications every 5 seconds
    setInterval(function() {
        notificationGetAll();
    }, 5000);

    // Get notification counter
    notificationNewCounter();

    // Get notification counter every 5 seconds
    setInterval(function() {
        notificationNewCounter();
    }, 5000);

    // Update notification when notification nav is clicked
    $('#notif-icon').on('click', updateNotification);
});

function notificationGetAll() {
    $.ajax({
        url: "/notifications/get_all",
        method: "GET"
    })
        .always(function (response) {
            $('#notif-loader').html(response);
        });
}

function notificationNewCounter() {
    $.ajax({
        url: "/notifications/new_counter",
        method: "GET"
    })
        .always(function(response) {
            if (response > 0)
            {
                $('#notif-new-counter').html(response);
            }
        });
}

function updateNotification() {
    $.ajax({
        url: "/notifications/read_all",
        method: "POST"
    })
        .always(function(response) {
            $('#notif-new-counter').html('');
        });
}

function showDeleteModalViolation(e) {
    e.preventDefault();

    var _this = $(this);
    var slug = _this.data('slug');

    _this.html('<i class="fa fa-spin fa-spinner"></i>');
    _this.addClass('disabled');
    _this.attr('disabled','disabled');

    $.ajax({
        url: 'violations/' + slug + '/load_modal',
        type: 'GET'
    })
        .always(function(result) {
            modalTitle.addClass('text-danger');
            modalTitle.html('DELETE');
            smallModalContent.html(result);
            smallModal.modal('show');

            _this.html('<i class="fa fa-times"></i>');
            _this.removeClass('disabled');
            _this.removeAttr('disabled');
        });
}

function showDeleteModalUser(e) {
    e.preventDefault();

    var _this = $(this);
    var slug = _this.data('slug');

    _this.html('<i class="fa fa-spin fa-spinner"></i>');
    _this.addClass('disabled');
    _this.attr('disabled','disabled');

    $.ajax({
        url: 'users/' + slug + '/load_modal',
        type: 'GET'
    })
        .always(function(result) {
            modalTitle.addClass('text-danger');
            modalTitle.html('DELETE');
            smallModalContent.html(result);
            smallModal.modal('show');

            _this.html('<i class="fa fa-times"></i>');
            _this.removeClass('disabled');
            _this.removeAttr('disabled');
        });
}

function getViolationTable(category, load)
{
    if (load)
    {
        $('#violation-list').html('<div class="spinner"><i class="fa fa-spin fa-spinner"></i></div>');
    }

    var query = (category !== '') ? ("?cat=" + category) : "";

    $.ajax({
        url: "violations/table" + query,
        method: "GET"
    })
        .always(function(result) {
            $('#violation-list').html(result);
        });
}

function loadViolationByCategory(e)
{
    e.preventDefault();

    $('.li-violation-category').removeClass('active');

    $(this).addClass('active');

    var category = $(this).val();

    currentCategory = category;

    getViolationTable(category, true);
}