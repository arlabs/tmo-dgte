<li class="{{ Active::pattern(['violations','violations/*']) }}">
    <a href="{{ url('violations') }}">
        <i class="fa fa-file-text"></i> <span>Violations</span>
    </a>
</li>
<li class="{{ Active::pattern(['traffic','traffic/*']) }}">
    <a href="{{ url('traffic') }}">
        <i class="fa fa-file-text"></i> <span>Traffic Info</span>
    </a>
</li>
<li class="{{ Active::pattern(['users','users/*']) }} hidden">
    <a href="{{ url('users') }}">
        <i class="fa fa-file-text"></i> <span>Users</span>
    </a>
</li>
