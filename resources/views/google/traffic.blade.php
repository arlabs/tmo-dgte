@extends('front')

@section('content')
    <!-- container -->
    <div class="container">
        <div class="row">

            <!-- Article main content -->
            <article class="col-sm-12 maincontent">
                <header class="page-header">
                    <h2 class="page-title">Traffic Information</h2>
                </header>

                <div id="map"></div>
                <hr/>
                <div>
                    <h4>Understand color meanings<span style="font-weight: normal; font-size: 14px;"> &mdash; <em>By <a href="https://support.google.com/maps/answer/3093389?hl=en" target="_blank">Google</a></em></span></h4>
                    <p>The colors indicate the speed of traffic on the road.</p>
                    <ul>
                        <li><span class="label label-success">Green</span> means there are no traffic delays.</li>
                        <li><span class="label label-orange">Orange</span> means there's a medium amount of traffic.</li>
                        <li><span class="label label-danger">Red</span> means there are traffic delays. The more red, the slower the speed of traffic on the road.</li>
                    </ul>
                    <p>If you see <span class="label label-default">Gray</span> or <span class="label label-primary">Blue</span> lines on the map, these lines are your directions routes.</p>
                </div>
            </article>
            <!-- /Article -->

        </div>
    </div>	<!-- /container -->
@stop

@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXjzE1x5vkAO54CbVi7cat5n3tZkzvxO4&callback=initMap&signed_in=true" async defer></script>
<script>
    $(function() {
        // Refresh map every 30 seconds
        setInterval(
            function () {
                initMap();
            }, 30000);
    });

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: {lat: 9.30535108501075, lng: 123.30679168546135} // Dumaguete Long/Lat
            //center: {lat: 34.04924594193164, lng: -118.24104309082031}
        });

        var trafficLayer = new google.maps.TrafficLayer();
        trafficLayer.setMap(map);
    }
</script>
@stop