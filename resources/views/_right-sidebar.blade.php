<div class="widget">
    <h3>Emergency Contacts:</h3>
    <ul class="list-unstyled list-spaces">
        <li>
            <a href="#">Traffic Management Office</a><br>
            <span class="small text-muted">225-1662</span><br/>
            <span class="small text-muted">422-2583</span>
        </li>
        <li>
            <a href="#">Police Department</a><br>
            <span class="small text-muted">(035) 225-1766</span><br/>
            <span class="small text-muted">0929-200-6999</span><br/>
            <span class="small text-muted">0917-933-0022</span>
        </li>
        <li>
            <a href="#">Prov PNP</a><br>
            <span class="small text-muted">225-2819</span><br/>
            <span class="small text-muted">419-8332</span>
        </li>
        <li>
            <a href="#">Fire Department</a><br>
            <span class="small text-muted">225-3445</span><br/>
            <span class="small text-muted">422-9672</span>
        </li>
        <li>
            <a href="#">NORECO 2</a><br>
            <span class="small text-muted">225-4830</span><br/>
            <span class="small text-muted">422-6522</span>
        </li>
        <li>
            <a href="#">Fil-Chinese Fire Volunteers</a><br>
            <span class="small text-muted">0922-825-0991</span><br/>
            <span class="small text-muted">0905-372-8855</span>
        </li>
        <li>
            <a href="#">City Disaster & Rescue</a><br>
            <span class="small text-muted">226-3483</span><br/>
            <span class="small text-muted">0905-938-3110</span>
        </li>
        <li>
            <a href="#">SU Medical</a><br>
            <span class="small text-muted">(035)225-0841</span><br/>
            <span class="small text-muted">422-7166</span><br/>
            <span class="small text-muted">422-7180</span>
        </li>
        <li>
            <a href="#">Holy Child Hospital</a><br>
            <span class="small text-muted">225-0510</span><br/>
            <span class="small text-muted">226-1113</span>
        </li>
        <li>
            <a href="#">NegOr Provincial Hospital</a><br>
            <span class="small text-muted">225-9286</span>
        </li>
        <li>
            <a href="#">Red Cross</a><br>
            <span class="small text-muted">225-2835</span><br/>
            <span class="small text-muted">422-4553</span>
        </li>
        <li>
            <a href="#">One Rescue EMS</a><br>
            <span class="small text-muted">225-9110</span><br/>
            <span class="small text-muted">422-9110</span>
        </li>
        <li>
            <a href="#">Tourist Assistance</a><br>
            <span class="small text-muted">225-0549</span><br/>
            <span class="small text-muted">422-0675</span>
        </li>
    </ul>
</div>