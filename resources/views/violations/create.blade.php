@extends('back')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>

    <div class="content">
        <div class="box">
            <article class="box-body">
                <div class="row">
                    <div class="col-sm-5">
                        {!! Form::open(['url' => 'violations']) !!}

                        @include('violations._form', ['admin' => Auth::user()->isAdmin()])

                        {!! Form::close() !!}
                    </div>
                </div>
            </article>
        </div>
    </div>

@stop