@extends('back')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>

    <div class="content">

        <div class="box">
            <article class="box-body">
                {!! Form::open(['url' => 'violations/user/issue']) !!}

                @include('violations._form_issue')

                {!! Form::close() !!}
            </article>
        </div>
    </div>
@stop