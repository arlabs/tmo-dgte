@extends('back')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>

    <div class="content">
        <div class="box">
            <article class="box-body">

                <div class="row">
                    <div class="col-sm-4">
                        {!! Form::open(['url' => 'violations/'.$violation->slug]) !!}

                        @include('violations._form', ['admin' => Auth::user()->isAdmin()])

                        {!! Form::close() !!}
                    </div>

                    <div class="col-sm-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Persons committed this violation</h3>
                            </div>
                            <div class="panel-body">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Location</th>
                                        <th>Date Happened</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @if($violation->users->count())
                                            @foreach($violation->users as $user)
                                                <tr>
                                                    <td>{{ $user->pivot->user_id }}</td>
                                                    <td>{{ ucwords($user->profile->first_name.' '.$user->profile->last_name) }}</td>
                                                    <td>{{ ucwords($user->pivot->location) }}</td>
                                                    <td>{{ $user->pivot->created_at->format('d-M-Y h:i A') }}</td>
                                                    <td>{{ ucwords($user->pivot->status) }}</td>
                                                    <td>
                                                        <a href="{{ url('violations/user/print_violation?user_id='.$user->pivot->user_id.'&violation_id='.$user->pivot->violation_id) }}" target="_blank" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Print"><i class="fa fa-print"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr><td colspan="6">No persons committed this violation</td></tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <!-- /Article -->
        </div>
    </div>	<!-- /container -->
@stop