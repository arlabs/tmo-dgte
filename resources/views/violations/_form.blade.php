
<div class="form-group {{ $errors->has('violation_category') ? 'has-error' : '' }}">
    {!! Form::label('violation_category', 'Category:') !!}
    {!! Form::select('violation_category', $categories, $violation ? $violation->violation_category_id : null, ['class' => 'form-control', ($admin ? '' : 'disabled')]) !!}
    {!! $errors->first('violation_category', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {!! Form::label('name', 'Violation Name:') !!}
    {!! Form::text('name',$violation ? ucwords($violation->name) : null,['class' => 'form-control', ($admin ? '' : 'disabled'), 'placeholder' => '']) !!}
    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', $violation ? $violation->description : null,['class' => 'form-control', ($admin ? '' : 'disabled'), 'rows' => 5, 'placeholder' => '(Optional)']) !!}
</div>

<div class="form-group {{ $errors->has('penalty') ? 'has-error' : '' }}">
    {!! Form::label('penalty', 'Penalty:') !!}
    {!! Form::number('penalty', $violation ? $violation->penalty : null, ['class' => 'form-control', ($admin ? '' : 'disabled'), 'placeholder' => '']) !!}
    {!! $errors->first('penalty', '<span class="help-block">:message</span>') !!}
</div>

@if($admin)
<div class="row">
    <div class="col-sm-6"></div>
    <div class="col-sm-6"><button type="submit" class="btn btn-primary pull-right">Submit</button></div>
</div>
@endif