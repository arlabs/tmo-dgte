<table class="table table-hover table-violations">
    <thead>
    <tr>
        <th>ID</th>
        <th>Violation Name</th>
        <th>Penalty</th>
        <th class="text-center">
            <abbr data-toggle="tooltip" title="Number of persons committed the violation {{ Auth::user()->isAdmin() ? '' : '(including you)' }}">Persons</abbr>
        </th>
        <th>Date Registered</th>
        @if (Auth::user()->isAdmin())
            <th style="width: 10%;">Action</th>
        @endif
    </tr>
    </thead>
    <tbody>
        @if ($violations->count())
            @foreach($violations as $violation)
                <tr>
                    <td><a href="{{ url('violations/'.$violation->slug) }}">{{ str_pad($violation->id, 5, 0, STR_PAD_LEFT) }}</a></td>
                    <td>{{ ucwords($violation->name) }}</td>
                    <td style="text-align: right;">&#8369;{{ number_format($violation->penalty,2) }}</td>
                    <td class="text-center">{{ $violation->users->count() }}</td>
                    <td>{{ $violation->created_at->format('d-M-Y') }}</td>
                    @if (Auth::user()->isAdmin())
                        <td style="text-align: center;">
                            <a href="#" data-slug="{{ $violation->slug }}" class="btn btn-danger btn-sm btn-delete-violation"><i class="fa fa-times"></i></a>
                        </td>
                    @endif
                </tr>
            @endforeach
        @else
            <tr><td colspan="7">No violations available</td></tr>
        @endif
    </tbody>
</table>

@if( ! Auth::user()->isAdmin())
    {!! $violations->render() !!}
@endif

<script>
    $(function() {
        var table = $('.table-violations').dataTable({
            "sDom": '<"top"f>t<"bottom"p><"clear">',
            "aaSorting": [[idx,dir]]
        });

        $(".table-violations thead").on("click", "th", function (event) {
           idx = table.fnSettings().aaSorting[0][0];
           dir = table.fnSettings().aaSorting[0][1];
        });
    });
</script>