@extends('back')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="pull-right">
            @if (Auth::user()->isAdmin())
                <a href="{{ url('violations/create') }}" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> New Violation</a>
                <a href="{{ url('violations/user/issue') }}" class="btn btn-default btn-sm"><i class="fa fa-file-text"></i> Issue Violation</a>
            @endif
        </div>

        <h1>{{ $title }}</h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box">
            <!-- Article main content -->
            <article class="box-body">

                <div id="violation-tabs">

                    @if(Auth::user()->isAdmin())
                    <form class="form-inline" style="margin-bottom: -30px;">
                        <div class="form-group">
                            <label>Filter:</label>
                            <select class="form-control input-sm li-violation-category">
                                <option value="">All</option>
                                @foreach($categories as $index => $cat)
                                    <option value="{{ $cat->slug }}">{{ ucwords($cat->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                    @endif

                    <!-- Violation List -->
                    @if(Auth::user()->isAdmin())
                        <div id="violation-list" role="tabpanel" class="tab-pane fade in active"></div>
                    @else
                        @include('violations._table')
                    @endif

                </div>

            </article>
        </div>
    </section>
@stop

@section('scripts')
<script>
$(function() {

});
</script>
@endsection