<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('violation') ? 'has-error' : '' }}">
            {!! Form::label('violation', 'Violation:') !!}
            {!! Form::select('violation', $categorizedViolationList, null, ['class' => 'form-control']); !!}
            {!! $errors->first('violation', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group {{ $errors->has('user') ? 'has-error' : '' }}">
            {!! Form::label('user', 'User:') !!}
            {!! Form::select('user', $users, null, ['class' => 'form-control']) !!}
            {!! $errors->first('user', '<span class="help-block">:message</span>') !!}
            <span class="help-block">User not registered?
                <a href="{{ url('users/create/new_user_violation') }}">Create</a> his/her account first.</span>
        </div>

        <div class="form-group {{ $errors->has('location') ? 'has-error' : '' }}">
            {!! Form::label('location', 'Location:') !!}
            {!! Form::text('location', null, ['class' => 'form-control', 'placeholder' => '']) !!}
            {!! $errors->first('location', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group {{ $errors->has('happened_at') ? 'has-error' : '' }}">
            {!! Form::label('happened_at', 'Date Happened:') !!}
            <div class="input-group date" id="happened_at">
                {!! Form::text('happened_at', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                <span class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </span>
            </div>
            {!! $errors->first('happened_at', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
            {!! Form::label('status', 'Status:') !!}
            {!! Form::select('status', ['unpaid' => 'Unpaid', 'paid' => 'Paid'], null, ['class' => 'form-control']) !!}
            {!! $errors->first('status', '<span class="help-block">:message</span>') !!}
        </div>
    </div>

    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Vehicle Details</h3>
            </div>
            <div class="panel-body">

                <div class="form-group {{ $errors->has('registration_number') ? 'has-error' : '' }}">
                    {!! Form::label('registration_number', 'Registration Number:') !!}
                    {!! Form::text('registration_number', null, ['class' => 'form-control']); !!}
                    {!! $errors->first('registration_number', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group {{ $errors->has('official_receipt') ? 'has-error' : '' }}">
                    {!! Form::label('official_receipt', 'Official Receipt:') !!}
                    {!! Form::text('official_receipt', null, ['class' => 'form-control']); !!}
                    {!! $errors->first('official_receipt', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                    {!! Form::label('type', 'Type:') !!}
                    {!! Form::text('type', null, ['class' => 'form-control']); !!}
                    {!! $errors->first('type', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group {{ $errors->has('make') ? 'has-error' : '' }}">
                    {!! Form::label('make', 'Make:') !!}
                    {!! Form::text('make', null, ['class' => 'form-control']); !!}
                    {!! $errors->first('make', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group {{ $errors->has('plate_number') ? 'has-error' : '' }}">
                    {!! Form::label('plate_number', 'Plate No.:') !!}
                    {!! Form::text('plate_number', null, ['class' => 'form-control']); !!}
                    {!! $errors->first('plate_number', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group {{ $errors->has('bar_code') ? 'has-error' : '' }}">
                    {!! Form::label('bar_code', 'Barcode:') !!}
                    {!! Form::text('bar_code', null, ['class' => 'form-control']); !!}
                    {!! $errors->first('bar_code', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group {{ $errors->has('year_model') ? 'has-error' : '' }}">
                    {!! Form::label('year_model', 'Year Model:') !!}
                    {!! Form::text('year_model', null, ['class' => 'form-control']); !!}
                    {!! $errors->first('year_model', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group {{ $errors->has('motor_number') ? 'has-error' : '' }}">
                    {!! Form::label('motor_number', 'Motor No.:') !!}
                    {!! Form::text('motor_number', null, ['class' => 'form-control']); !!}
                    {!! $errors->first('motor_number', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group {{ $errors->has('chassis_number') ? 'has-error' : '' }}">
                    {!! Form::label('chassis_number', 'Chassis No.:') !!}
                    {!! Form::text('chassis_number', null, ['class' => 'form-control']); !!}
                    {!! $errors->first('chassis_number', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="form-group {{ $errors->has('dlr_number') ? 'has-error' : '' }}">
                    {!! Form::label('dlr_number', 'DLR No.:') !!}
                    {!! Form::text('dlr_number', null, ['class' => 'form-control']); !!}
                    {!! $errors->first('dlr_number', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6"></div>
    <div class="col-sm-6"><button type="submit" class="btn btn-primary pull-right">Submit</button></div>
</div>