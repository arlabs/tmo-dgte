<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="Sergey Pozhilov (GetTemplate.com)">
	<meta name="csrf-token" content="{{ csrf_token() }}" />

	<title>Traffic Violation Monitoring System</title>

	<link rel="shortcut icon" href="/images/gt_favicon.png">

    <link href="{{ elixir('css/libs.css') }}" rel="stylesheet">
    <link href="{{ elixir('css/front.css') }}" rel="stylesheet">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

	<script src="{{ asset('js/jquery-1.11.3.min.js') }}"></script>
</head>

<body class="home">
	<!-- Fixed Navbar -->
	@include('includes.fixed-nav')

    <!-- Flash Message -->
	@if (session('message'))
        @include('flash-message.template')
    @endif

	@yield('content')

	<!-- Modals -->
	@include('modals.modals')

    <!-- Footer -->
    @include('includes.footer')

	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	@yield('scripts')
	<script src="{{ elixir('js/libs.js') }}"></script>
	<script src="{{ elixir('js/front.js') }}"></script>
	<script>
	    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
	</script>
</body>
</html>