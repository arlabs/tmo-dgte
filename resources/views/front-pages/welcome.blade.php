@extends('front')

@section('content')

    <!-- Header -->
    <header id="head">
        <div class="container">
            <div class="row">
                <h1 class="lead">Real-Time Vehicular Traffic Violation Transmission System</h1>
                <p class="tagline">in Traffic Monitoring for TMO Dumaguete</p>
                <p>
                    <a href="{{ url('auth/login') }}" class="btn btn-transparent btn-alpha btn-lg" role="button">SIGN IN</a>
                </p>
            </div>
        </div>
    </header>
    <!-- /Header -->

@stop