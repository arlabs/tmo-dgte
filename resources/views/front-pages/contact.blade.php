@extends('front')

@section('content')
    <!-- container -->
    <div class="container">
        <div class="row">

            <!-- Article main content -->
            <article class="col-sm-9 maincontent">
                <header class="page-header">
                    <h1 class="page-title">Contact us</h1>
                </header>

                <div class="widget">
                    <h4>Address:</h4>
                    <address>
                        Dumaguete City
                    </address>
                    <h4>Phone:</h4>
                    <address>
                        225-1662<br/>
                        422-2538
                    </address>
                </div>
            </article>
            <!-- /Article -->

            <!-- Sidebar -->
            <aside class="col-sm-3 sidebar sidebar-right">

                @include('_right-sidebar')

            </aside>
            <!-- /Sidebar -->

        </div>
    </div>	<!-- /container -->

    <section class="container-full top-space">
        <div id="map"></div>
    </section>

@stop