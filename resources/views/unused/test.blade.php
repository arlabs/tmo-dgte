@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['url' => 'test']) !!}
                    <div class="form-group">
                        {!! Form::text('name',null,['class' => 'form-control', 'placeholder' => 'Text (test)']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::email('email',null,['class' => 'form-control', 'placeholder' => 'Email (test)']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::password('password',['class' => 'form-control', 'placeholder' => 'Password (test)']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::number('total',null,['class' => 'form-control', 'placeholder' => 'Number (test)']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::date('date_hired',null,['class' => 'form-control', 'placeholder' => 'Date (test)']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::checkbox('date_hired',null,true) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::radio('date_hired',null,true) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::file('image') !!}
                    </div>
                    <div class="form-group">
                        {!! Form::select('size', ['L' => 'Large', 'S' => 'Small'], 'S', ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::select('animal', [
                            'Cats' => ['leopard' => 'Leopard'],
                            'Dogs' => ['spaniel' => 'Spaniel'],
                        ], null, ['class' => 'form-control']); !!}
                    </div>
                    <div class="form-group">
                        {!! Form::selectRange('number', 10, 20, null, ['class' => 'form-control']); !!}
                    </div>
                    <div class="form-group">
                        {!! Form::selectMonth('month', null, ['class' => 'form-control']); !!}
                    </div>

                    {!! Form::submit('Submit', ['class' => 'form-control']); !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop