<div class="alert alert-{{ session('alert-level') }} alert-flat alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

    @if(session('alert-level') == 'success')
        <h4><i class="icon fa fa-check"></i> Success!</h4>
    @elseif(session('alert-level') == 'warning')
        <h4><i class="icon fa fa-warning"></i> Warning!</h4>
    @elseif(session('alert-level') == 'danger')
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
    @else
        <h4><i class="icon fa fa-info"></i> Info!</h4>
    @endif

    {!! session('message') !!}
</div>