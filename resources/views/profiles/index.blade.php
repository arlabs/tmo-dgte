@extends('back')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <a href="{{ url('profile/edit') }}" class="btn btn-success btn-sm pull-right hidden" style="display: none;"><i class="fa fa-edit"></i> Edit</a>
        <h1>{{ $title }}</h1>
    </section>

    <div class="content">
        <div class="box">

            <!-- Article main content -->
            <article class="box-body">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <img src="/images/mac.jpg" class="img-responsive img-thumbnail" />
                    </div>
                    <div class="col-md-5 col-sm-12">
                        <div class="profile-body">
                            <h2>{{ ucwords($user->profile->first_name.' '.$user->profile->middle_name.' '.$user->profile->last_name) }}</h2>
                            <p><i class="fa fa-envelope"></i> {{ $user->email }}</p>
                            <p><i class="fa fa-phone"></i> {{ $user->profile->telephone_number }}</p>
                            <p><i class="fa fa-map-marker"></i> {{ ucwords($user->profile->address) }}</p>

                            <header class="page-header">
                                <h3 class="page-title">License Details</h3>
                            </header>
                            <p><strong>License Type:</strong> {{ ucwords($user->profile->license_type) }}</p>
                            <p><strong>License No.:</strong> {{ ucwords($user->profile->license_number) }}</p>
                            <p><strong>Date Issued:</strong> {{ $user->profile->license_date_issued->format('d-M-Y') }}</p>
                        </div>
                    </div>
                </div>
            </article>
            <!-- /Article -->

        </div>
    </div>	<!-- /container -->

@stop