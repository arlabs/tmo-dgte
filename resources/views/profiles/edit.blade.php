@extends('back')

@section('content')
    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>

    <div class="content">
        <div class="box">

            <article class="box-body">

                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::open(['url' => 'profile/update']) !!}

                        @include('users._form', ['create' => false])

                        {!! Form::close() !!}
                    </div>
                </div>
            </article>

        </div>
    </div>

@stop