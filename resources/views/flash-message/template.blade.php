<div class="alert alert-{{ session('alert-level') }}">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

    {!! session('message') !!}
</div>