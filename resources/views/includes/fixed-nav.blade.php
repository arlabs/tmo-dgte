<div class="navbar navbar-inverse navbar-fixed-top headroom" >
    <div class="container">
        <div class="navbar-header">
            <!-- Button for smallest screens -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="/images/dgtecity-logo.png" width="70" height="70" alt="Traffic Management Office - Dumaguete City">
            </a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::guest())
                    <li class="{{ Active::pattern('/') }}"><a href="{{ url('/') }}">Home</a></li>
                    <li class="{{ Active::pattern('violation-information') }}">
                        <a href="{{ url('violation-information') }}">Violations</a>
                    </li>
                    <li class="{{ Active::pattern('about') }}"><a href="{{ url('about') }}">About</a></li>
                    <li class="{{ Active::pattern('faq') }}"><a href="{{ url('faq') }}">FAQ</a></li>
                    <li class="{{ Active::pattern('contact') }}"><a href="{{ url('contact') }}">Contact</a></li>
                    <li><a class="btn" href="{{ url('auth/login') }}">Sign in</a></li>
                @endif

                @if(Auth::check())
                    <li class="{{ Active::pattern('dashboard') }}"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                    <li class="{{ Active::pattern('traffic') }}"><a href="{{ url('traffic') }}">Traffic Info</a></li>
                @endif
            </ul>
        </div>
    </div>
</div>

<header id="head" class="secondary"></header>