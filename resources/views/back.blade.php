<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{{ $title }} | TMO</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <meta name="_token" content="{{ csrf_token() }}" />

        <link href="{{ elixir('css/libs.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ elixir('css/back.css') }}" rel="stylesheet" type="text/css">
        @yield('styles')

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

	    <script src="{{ asset('js/jquery-1.11.3.min.js') }}"></script>
	    <script>
	        var idx = 0;
	        var dir = 'desc';
	    </script>
    </head>
    <body class="skin-orange fixed">
        <!-- Site wrapper -->
        <div class="wrapper">

            @include('inclusions.back.header')

            @include('inclusions.back.sidebar')

            <div class="content-wrapper">
                <section class="content-header">
                    @if (session('message'))
                        @include('flash.template')
                    @endif
                </section>

                @yield('content')
            </div>

            @include('inclusions.back.footer')
        </div>

        @include('generic.modals')

        <script src="{{ elixir('js/libs.js') }}"></script>
        <script src="{{ elixir('js/back.js') }}"></script>
        <script>
            $(function () {
                /*$('input').iCheck({
                    checkboxClass: 'icheckbox_flat-orange',
                    radioClass: 'iradio_flat-orange',
                    increaseArea: '20%' // optional
                });*/

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-Token': $('meta[name="_token"]').attr('content')
                    }
                });
            });
        </script>
        @yield('scripts')
    </body>
</html>