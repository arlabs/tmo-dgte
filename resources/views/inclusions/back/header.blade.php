<header class="main-header">
    <a href="{{ url('/') }}" class="logo">TMO</a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu" style="display: none;">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-envelope"></i>
                    <span class="label label-success">4</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 4 messages</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <li><!-- start message -->
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="/images/avatar.png" class="img-circle" alt="User Image"/>
                                        </div>
                                        <h4>
                                            Support Team
                                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li><!-- end message -->
                            </ul>
                        </li>
                        <li class="footer"><a href="#">See All Messages</a></li>
                    </ul>
                </li>

                <!-- Notifications: style can be found in dropdown.less -->
                @if(Auth::user()->isAdmin())
                <li class="dropdown notifications-menu">
                    <a href="#" id="notif-icon" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell"></i>
                        <span id="notif-new-counter" class="label label-danger"></span>
                    </a>
                    <ul class="dropdown-menu" id="notif-loader"></ul>
                </li>
                @endif

                <li class="user user-menu" data-toggle="tooltip" data-placement="bottom" title="Profile">
                    <a href="{{ url('profile') }}">
                        <img src="/images/avatar.png" class="user-image" alt="User Image"/>
                        <span class="hidden-xs">{{ ucwords(Auth::user()->profile->first_name) }}</span>
                    </a>
                </li>
                <li class="notifications-menu" data-toggle="tooltip" data-placement="bottom" title="Logout">
                    <a href="{{ url('auth/logout') }}">
                        <i class="fa fa-sign-out"></i>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>