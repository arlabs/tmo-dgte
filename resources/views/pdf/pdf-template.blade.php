<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">

	<title>Traffic Violation Monitoring System</title>

	<style>
	    body {
	        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	        position: relative;
	    }
        body .container {
            padding: 10px;
            border: 1px solid #DDDDDD;
        }

	    table {
	        width: 100%;
	    }
	        table tbody tr:first-child td,
	        table tbody tr.force-border td {
	            border-bottom: 1px solid #DDDDDD;
	        }
	        table tbody tr td {
	            text-align: center;
	        }

        .page-break {
            page-break-after: always;
        }

        .pusher {
            height: 20px;
        }

        .text-center {
            text-align: center;
        }

        .doc-title {
            padding: 5px;
            background-color: #353535;
            color: #DDDDDD;
        }

        .value {
            font-weight: bold;
        }

        .logo-right {
            position: absolute;
            top: 2px;
            right: 2px;
        }
    </style>

</head>

<body>
    <div class="container">
	    @yield('content')
    </div>
</body>
</html>