@extends('pdf.pdf-template')

@section('content')

    <div class="logo">
        <img src="images/dgtecity-logo.png" class="logo-right" width="100" height="100">
    </div>

    <div class="text-center">
        <h4>Republic of the Philippines</h4>
        <h4>City of Dumaguete</h4>
        <h2>TRAFFIC MANAGEMENT OFFICE</h2>
    </div>

    <div class="pusher"></div>

    <div class="doc-title">
        <p>{{ ucwords($violation->category->name) }}</p>
    </div>

    <div class="pusher"></div>

    <table>
        <tbody>
        <tr>
            <td class="value">{{ ucwords($user->profile->last_name) }}</td>
            <td class="value">{{ ucwords($user->profile->first_name) }}</td>
            <td class="value">{{ ucwords($user->profile->middle_name) }}</td>
        </tr>
        <tr>
            <td>LAST</td>
            <td>FIRST</td>
            <td>MIDDLE</td>
        </tr>
        </tbody>
    </table>

    <div class="pusher"></div>

    <table>
        <tbody>
        <tr>
            <td width="10">ADDRESS: </td>
            <td class="value" style="text-align: left;">{{ ucwords($user->profile->address) }}</td>
        </tr>
        </tbody>
    </table>

    <div class="pusher"></div>

    <table>
        <tbody>
        <tr>
            <td class="value">{{ ucwords($user->profile->license_type) }}</td>
            <td class="value">{{ ucwords($user->profile->license_number) }}</td>
            <td class="value">{{ $user->profile->license_date_issued->format('d-M-Y') }}</td>
        </tr>
        <tr>
            <td>LICENSE TYPE</td>
            <td>LICENSE NUMBER</td>
            <td>DATE ISSUED</td>
        </tr>
        </tbody>
    </table>

    <div class="pusher"></div>

    <table>
        <tbody>
        <tr>
            <td class="value">{{ ucwords($vehicle->registration_number) }}</td>
            <td class="value">{{ ucwords($vehicle->official_receipt) }}</td>
        </tr>
        <tr>
            <td>REGISTRATION NO.</td>
            <td>OFFICIAL RECEIPT</td>
        </tr>
        </tbody>
    </table>

    <div class="pusher"></div>

    <table>
        <tbody>
        <tr>
            <td class="value">{{ ucwords($vehicle->type) }}</td>
            <td class="value">{{ ucwords($vehicle->make) }}</td>
            <td class="value">{{ ucwords($vehicle->plate_number) }}</td>
            <td class="value">{{ $vehicle->year_model }}</td>
        </tr>
        <tr>
            <td>TYPE</td>
            <td>MAKE</td>
            <td>PLATE</td>
            <td>YEAR MODEL</td>
        </tr>
        </tbody>
    </table>

    <div class="pusher"></div>

    <table>
        <tbody>
        <tr>
            <td class="value">{{ $vehicle->bar_code }}</td>
            <td class="value">{{ $vehicle->motor_number }}</td>
            <td class="value">{{ $vehicle->chassis_number }}</td>
            <td class="value">{{ $vehicle->dlr_number }}</td>
        </tr>
        <tr>
            <td>BARCODE</td>
            <td>MOTOR NO.</td>
            <td>CHASSIS NO.</td>
            <td>DLR NO.</td>
        </tr>
        </tbody>
    </table>

    <div class="pusher"></div>

    <table>
        <tbody>
        <tr>
            <td class="value" width="10"><h3>VIOLATION: </h3></td>
            <td class="value" style="text-align: left;"><h3>{{ ucwords($violation->name) }}</h3></td>
        </tr>
        <tr class="force-border">
            <td class="value" width="10"><h3>PENALTY: </h3></td>
            <td class="value" style="text-align: left;"><h3>PHP{{ number_format($violation->penalty,2) }}</h3></td>
        </tr>
        <tr class="force-border">
            <td class="value" width="10"><h3>STATUS: </h3></td>
            <td class="value" style="text-align: left;"><h3>{{ ucwords($violation->pivot->status) }}</h3></td>
        </tr>
        </tbody>
    </table>

    <div class="pusher"></div>

    <table>
        <tbody>
        <tr>
            <td class="value">{{ ucwords($violation->pivot->location) }}</td>
            <td class="value">{{ $violation->pivot->created_at->format('d-M-Y') }}</td>
            <td class="value">{{ $violation->pivot->created_at->format('h:i A') }}</td>
        </tr>
        <tr>
            <td>PLACE</td>
            <td>DATE</td>
            <td>TIME</td>
        </tr>
        </tbody>
    </table>

@stop