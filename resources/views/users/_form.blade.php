{!! Form::hidden('purpose',$purpose) !!}

<div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
    {!! Form::label('first_name', 'First Name:') !!}
    {!! Form::text('first_name',$user ? $user->profile->first_name : null,['class' => 'form-control', 'placeholder' => '']) !!}
    {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('middle_name') ? 'has-error' : '' }}">
    {!! Form::label('middle_name', 'Middle Name:') !!}
    {!! Form::text('middle_name',$user ? $user->profile->middle_name : null,['class' => 'form-control', 'placeholder' => '']) !!}
    {!! $errors->first('middle_name', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
    {!! Form::label('last_name', 'Last Name:') !!}
    {!! Form::text('last_name',$user ? $user->profile->last_name : null,['class' => 'form-control', 'placeholder' => '']) !!}
    {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('telephone_number') ? 'has-error' : '' }}">
    {!! Form::label('telephone_number', 'Telephone No.:') !!}
    {!! Form::text('telephone_number',$user ? $user->profile->telephone_number : null,['class' => 'form-control', 'placeholder' => '']) !!}
    {!! $errors->first('telephone_number', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address',$user ? $user->profile->address : null,['class' => 'form-control', 'placeholder' => 'No. of Street, City/Municipality/Province']) !!}
    {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('license_type') ? 'has-error' : '' }}">
    {!! Form::label('license_type', 'License Type:') !!}
    {!! Form::select('license_type', ['student permit' => 'Student Permit','non-professional' => 'Non-professional','professional' => 'Professional'],$user ? $user->profile->license_type : null,['class' => 'form-control', 'placeholder' => '']) !!}
    {!! $errors->first('license_type', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('license_number') ? 'has-error' : '' }}">
    {!! Form::label('license_number', 'License No.:') !!}
    {!! Form::text('license_number',$user ? $user->profile->license_number : null,['class' => 'form-control', 'placeholder' => '']) !!}
    {!! $errors->first('license_number', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('license_date_issued') ? 'has-error' : '' }}">
    {!! Form::label('license_date_issued', 'License Date Issued:') !!}
    <div class="input-group date" id="license_date_issued">
        {!! Form::text('license_date_issued', $user ? $user->profile->license_date_issued->format('m-d-Y h:i A') : null, ['class' => 'form-control', 'placeholder' => '']) !!}
        <span class="input-group-addon">
            <span class="fa fa-calendar"></span>
        </span>
    </div>
    {!! $errors->first('license_date_issued', '<span class="help-block">:message</span>') !!}
</div>

@if ($create)
<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email',$user ? $user->email : null,['class' => 'form-control', 'placeholder' => '']) !!}
    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }} hidden">
    {!! Form::label('password', 'Password:') !!}
    <input type="password" name="password" value="123456">
    {{--{!! Form::password('password',['class' => 'form-control', 'value' => '123456', 'placeholder' => '']) !!}--}}
    {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }} hidden">
    {!! Form::label('password_confirmation', 'Confirm Password:') !!}
    <input type="password" name="password_confirmation" value="123456">
    {{--{!! Form::password('password_confirmation', ['class' => 'form-control', 'value' => '123456']) !!}--}}
    {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
</div>
@endif

<div class="form-group {{ $errors->has('role') ? 'has-error' : '' }} {{ $purpose == 'edit_profile' ? 'hidden' : '' }}">
    {!! Form::label('role', 'Roles:') !!}
    @if ($roles)
        @foreach($roles as $role)
            @if($role->name != 'admin')
                <div class="checkbox">
                    <label>
                        <input name="role[]"
                            type="checkbox"
                            value="{{ $role->id }}"
                            {{ $create ? ($role->name == 'driver' ? 'checked' : '') : (in_array($role->name, $user->roles->lists('name')) ? 'checked' : '') }}
                        > {{ ucwords($role->name) }}
                    </label>
                </div>
            @endif
        @endforeach
    @endif
    {!! $errors->first('role', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary pull-right">Submit</button>
</div>