@extends('back')

@section('content')
    <section class="content-header"><h1>{{ $title }}</h1></section>

    <div class="content">
        <div class="box">

            <!-- Article main content -->
            <article class="box-body">
                <header class="page-header">
                    <h2 class="page-title">Add New User</h2>
                </header>

                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::open(['url' => 'users']) !!}

                        @include('users._form', ['create' => true])

                        {!! Form::close() !!}
                    </div>
                </div>
            </article>
        </div>
    </div>
@stop