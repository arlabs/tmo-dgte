@extends('main')

@section('content')
    <!-- container -->
    <div class="container">
        <div class="row">
            <!-- Article main content -->
            <article class="col-sm-9 maincontent">
                <header class="page-header">
                    <h2 class="page-title">{{ ucwords($user->profile->first_name.' '.$user->profile->last_name) }}</h2>
                </header>

                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::open(['url' => 'users/'.$user->slug]) !!}

                        @include('users._form', ['create' => false])

                        {!! Form::close() !!}
                    </div>
                </div>
            </article>
            <!-- /Article -->

            <!-- Sidebar -->
            <aside class="col-sm-3 sidebar sidebar-right">

                @include('_right-sidebar')

            </aside>
            <!-- /Sidebar -->
        </div>
    </div>	<!-- /container -->
@stop