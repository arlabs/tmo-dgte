@extends('back')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <a href="{{ url('users/create/new_user') }}" class="btn btn-primary btn-sm pull-right" data-toggle="tooltip" title="Add new user"><i class="fa fa-plus"></i> Add</a>

        <h1>{{ $title }}</h1>
    </section>

    <div class="content">

        <div class="box">
            <article class="box-body">

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Date Joined</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if ($users->count())
                            @foreach($users as $user)
                                <tr>
                                    <td><a href="{{ url('users/'.$user->slug) }}">{{ str_pad($user->id, 5, 0, STR_PAD_LEFT) }}</a></td>
                                    <td>{{ ucwords($user->profile->first_name) }}</td>
                                    <td>{{ ucwords($user->profile->last_name) }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->created_at->format('d-M-Y') }}</td>
                                    <td>
                                        <a href="#" data-slug="{{ $user->slug }}" class="btn btn-danger btn-sm btn-delete-user" data-toggle="tooltip" title="Delete this user"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr><td colspan="6">No users available</td></tr>
                        @endif
                    </tbody>
                </table>

                {!! $users->render() !!}
            </article>
        </div>
    </div>
@stop