@if($notifications->count())
    <li>
        <ul class="menu">
            @foreach($notifications as $notif)
                <li class="{{ $notif->is_new ? 'active' : '' }}">
                    <a href="#"><i class="fa fa-users text-aqua"></i> {{ $notif->content }}</a>
                </li>
            @endforeach
        </ul>
    </li>
@endif

@unless($notifications->count())
    <li class="text-center"><a href="#">No notifications</a></li>
@endunless