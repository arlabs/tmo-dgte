@extends('auth.template')

@section('content')

    <div class="login-logo">
        <a href="#"><b>Reset Password</b></a>
    </div>

    <div class="login-box-body">
        <p class="login-box-msg">Please confirm your email address</p>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form role="form" method="POST" action="{{ url('/password/email') }}" class="on-submit-disable">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group has-feedback">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" autofocus>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-danger btn-block btn-flat">
                        <i class="fa fa-paper-plane"></i> Send Password Reset Link
                    </button>
                </div>
            </div>
        </form>
    </div>

@endsection
